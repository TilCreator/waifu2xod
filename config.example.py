default_values = {'url': 'https://static1.e621.net/data/cf/b9/cfb95794deaaafdfc0736d1a9614a5e2.png', 'scale': 2, 'noise': 3, 'model': 'models-cunet'}
max_scale = 8

urllib_headers = [('User-agent', 'waifu2x-upscaler')]

tmp_dir = '/run/waifu2xod/'

waifu2x_implemetation = 'waifu2x-ncnn-vulkan'  # Select 'waifu2x-ncnn-vulkan' or 'waifu2x-converter-cpp'
waifu2x_bin = '/usr/bin/waifu2x-ncnn-vulkan'
model_dir = '/usr/share/waifu2x-ncnn-vulkan/'

threads = '2:4:4'
gpu_id = 'auto'
tile_size = '0'
nice_level = 15
cache_time = 24 * 60 * 60  # 1day
cache_max_size = 3221225472  # 3gb
redo_timeout = 30

host_debug = 'localhost'
port_debug = 3200
