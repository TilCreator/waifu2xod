from flask import Flask, request, after_this_request, send_file, Response
from hashlib import md5
import subprocess
import urllib.request
import os
import time

import config

app = Flask(__name__)


def init():
    opener = urllib.request.build_opener()
    opener.addheaders = config.urllib_headers
    urllib.request.install_opener(opener)


@app.route('/')
def index():
    data = {}
    for key, default_value in config.default_values.items():
        if key in request.args:
            if key in ['scale', 'noise']:
                data[key] = int(request.args[key])
            else:
                data[key] = request.args[key]
        else:
            data[key] = default_value

    data['force'] = 'force' in request.args

    data['hash'] = md5((data['url'] + str(data['scale']) + str(data['noise'])).encode('utf-8')).hexdigest()

    result_path = os.path.join(config.tmp_dir, data['hash'])

    if data['scale'] > config.max_scale:
        return Response(f'Max scale is {config.max_scale}, your requested scale is {data["scale"]}', status=400, mimetype='text/plain')

    if not data['force'] and not os.path.exists(result_path) and os.path.exists(result_path + '_'):
        for _ in range(config.redo_timeout):
            time.sleep(1)

            if os.path.exists(result_path):
                break

    if not os.path.exists(result_path) or data['force']:
        if os.path.exists(result_path + '_'):
            os.remove(result_path + '_')

        urllib.request.urlretrieve(data['url'], result_path + '_')

        if config.waifu2x_implemetation == 'waifu2x-ncnn-vulkan':
            cmd = ['nice', '-n', str(config.nice_level), config.waifu2x_bin, '-v', '-j', config.threads, '-t', config.tile_size, '-g', config.gpu_id, '-f', 'png',
                   '-s', str(data['scale']), '-n', str(data['noise']), '-m', os.path.join(config.model_dir, data['model'].replace('.', '').replace('/', '')),
                   '-i', result_path + '_', '-o', result_path + '.png']
        elif config.waifu2x_implemetation == 'waifu2x-converter-cpp':
            cmd = ['nice', '-n', str(config.nice_level), config.waifu2x_bin, '-m', 'noise-scale', '-j', str(config.threads), '-f', 'png',
                   '--scale-ratio', str(data['scale']), '--noise-level', str(data['noise']),
                   '-i', result_path + '_', '-o', result_path + '.png']

        pipes = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        std_out, std_err = pipes.communicate()
        output = f'args: {" ".join(cmd)}\n\n\nreturn code:\n{pipes.returncode}\n\n\nstdout:\n{std_out.strip()}\n\n\nstderr:\n{std_err.strip()}'

        os.remove(result_path + '_')

        if pipes.returncode != 0:
            resp = Response(output, status=500, mimetype='text/plain')
        else:
            os.rename(os.path.join(config.tmp_dir, data['hash'] + '.png'), os.path.join(config.tmp_dir, data['hash']))

            resp = send_file(result_path, attachment_filename=data['url'][data['url'].rfind('/') + 1:data['url'].rfind('.')] + '.png')

        resp.headers['shell_output'] = output.replace('\n', "\\n")
    else:
        resp = send_file(os.path.join(config.tmp_dir, data['hash']), attachment_filename=data['url'][data['url'].rfind('/') + 1:data['url'].rfind('.')] + '.png')

    @after_this_request
    def after_request(response):
        files = []
        size = 0
        for file in os.listdir(config.tmp_dir):
            if file.endswith('_'):  # if file is an original and is possibly in use
                continue
            if os.path.getmtime(os.path.join(config.tmp_dir, file)) + config.cache_time < time.time():
                os.remove(os.path.join(config.tmp_dir, file))
                continue
            files.append((file, os.path.getmtime(os.path.join(config.tmp_dir, file))))
            size += os.path.getsize(os.path.join(config.tmp_dir, file))

        for file in [file[0] for file in sorted(files, key=lambda e: e[1])]:
            if size < config.cache_max_size:
                break
            size -= os.path.getsize(os.path.join(config.tmp_dir, file))
            os.remove(os.path.join(config.tmp_dir, file))

        return response

    if os.path.exists(result_path):
        os.utime(os.path.join(config.tmp_dir, data['hash']), (time.time(), time.time()))

    return resp


init()

if __name__ == '__main__':
    app.run(config.host_debug, port=config.port_debug, debug=True)
